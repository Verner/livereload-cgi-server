import os
import shutil
import subprocess
import tempfile

from bottle import Bottle, static_file, request, Response, HTTPError
from livereload import Server, shell
from logging import getLogger


logger = getLogger(__name__)

app = Bottle()

def _full_path(subdir, path):
    print(os.path.join(os.path.dirname(os.path.realpath(__file__)),subdir,path))
    return os.path.join(os.path.dirname(os.path.realpath(__file__)),subdir,path)


@app.route('/cgi-bin/<path:path>')
def cgi(path):
    env_predef = {}
    for _pt in [ 'HTTP_HOST',
                'HTTP_USER_AGENT',
                'QUERY_STRING',
                'REMOTE_HOST',
                'REMOTE_USER',
                'SERVER_NAME',
                'SERVER_PORT',
                'REQUEST_METHOD'
                ]:
        val = request.environ.get(_pt)
        if val is not None:
            env_predef[_pt] = val
    env_predef['PATH'] = os.environ.get('PATH')
    env_predef['SCRIPT_NAME'] = os.path.basename(path)
    env_predef['SCRIPT_FILENAME'] = path
    env_predef['SCRIPT_URI'] = request.environ.get('SCRIPT_NAME')+request.environ.get('PATH_INFO')
    env_predef['SERVER_SOFTWARE'] = __name__ # name of this module

    tmpfh = tempfile.NamedTemporaryFile()
    print(path)
    proc = subprocess.Popen(_full_path('cgi-bin',path),
            stdin=subprocess.PIPE,
            stdout=tmpfh,
            env=env_predef,
            cwd=os.path.dirname(_full_path('cgi-bin',path)))
    shutil.copyfileobj(request.body, proc.stdin)
    stdout, stderr = proc.communicate()
    rc = proc.wait()

    if logger is not None:
        if rc != 0:
            logger.err('CGI executable terminated with rc={0}'.format(rc))
        if stderr:
            for line in stderr.split('\n'):
                logger.err('CGI err: {0}'.format(line))

    if rc != 0:
        msgparts = ['CGI binary terminated with rc={0}.'.format(rc)]
        if stderr:
            msgparts.append(stderr)
        return HTTPError(status=500, body=msgparts)
    tmpfh.seek(0)
    #print(tmpfh.read())
    return Response(body=tmpfh)

@app.route('/<path:path>')
def static(path):
    return static_file(path, root='./')



server = Server(app)

def alert():
    print('foo')
server.watch('reload', alert)

server.serve()
